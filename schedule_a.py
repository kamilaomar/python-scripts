from xml.etree.ElementTree import Comment
import pandas as pd
import json
import numpy as np

import os

global db, dbc

CONF_UPDATES_ENABLED = False

def odbc_connect_CRM():
    import pyodbc
    return pyodbc.connect('Driver={SQL Server};Server=sqlserver;Database=CRM;UID=crm-tasks;PWD=rbutlerCR/\\/\\')
    # return pyodbc.connect('Driver={SQL Server};Server=sqlsandbox;Database=CRM;UID=crm-tasks;PWD=rbutlerCR/\\/\\')

def append_to_both_missing_list(row):
    file = open('both_missing.txt', 'a', encoding='utf-8')
    file.write(f'{row}\n')
    file.close()
    return

def append_to_one_missing_list(row):
    file = open('one_missing.txt', 'a', encoding='utf-8')
    file.write(f'{row}\n')
    file.close()
    return

def append_to_not_found_list(row):
    file = open('not_found.txt', 'a', encoding='utf-8')
    file.write(f'{row}\n')
    file.close()
    return

def update_oppo_vend_equipment(val, oppo_id):
    global db, dbc
    print("update_oppo_vend_equipment: "+str(oppo_id))
    print("val: "+val)
    # exit()
    if not CONF_UPDATES_ENABLED: return
    """
    db.execute('''
		update opportunity
		set oppo_vendorequipment = ?
		where oppo_opportunityid = ?
	''', (val, oppo_id))
    dbc.commit()
    """
    return

def append_1(row, sn, desc, type, subtype, oppo_id):
    file = open('found.txt', 'a', encoding='utf-8')

    file.write(f'original: {json.dumps(row)} with an oppo_id: {oppo_id}\n')

    for i in range(len(row)):
        for j in range(len(row[i])):
            
            if (len(row[i][j]) > 0):
                if (sn in row[i][j]['sn'] or desc in row[i][j]['desc']):
                    row[i][j]['type'] = type
                    row[i][j]['subtype'] = subtype

            file.write(f'updated with {json.dumps(row)}\n\n\n')
            
            # update_oppo_vend_equipment(json.dumps(row), oppo_id)

    file.close()

def append_to_any_txt(row):
    file = open('draft.txt', 'a', encoding='utf-8')
    file.write(f'{row}\n')
    file.close()
    return

def get_equip():
    global db, dbc

    dbc = odbc_connect_CRM()
    db = dbc.cursor()

    sql = '''
      select
        oppo_vendorequipment as equip, 
        oppo_opportunityid as oppo_id
      from opportunity
      where oppo_vendorequipment is not null
        and oppo_approval_lender like 'taycor%'
        and oppo_status in ('booked', 'funding', 'funded')
    '''
    oppo_vendorequipment = db.execute(sql).fetchall()

    equip_dict = [(json.loads(x[0]), x[1]) for x in oppo_vendorequipment]

    # for ea_row in oppo_vendorequipment:
    #     equip_dict = dict(zip(col_desc, ea_row))
    # col_desc = [x[0] for x in oppo_vendorequipment]
    # oppo_id = [x[1] for x in oppo_vendorequipment]

    # append_to_any_txt(col_desc)

    # return [col_desc, oppo_id]
    return equip_dict


dbc = odbc_connect_CRM()
db = dbc.cursor()

excel_file = pd.read_excel('C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/AT191/report.xlsx', sheet_name='Sheet2')

df = pd.DataFrame(excel_file, columns = ['sn','desc','type','subtype'])

df = df.fillna('')

# str = "1616-2846\nAvenge Pro CS04GL \nDo everything stain remover\n"


# sql = 'select oppo_vendorequipment from opportunity where oppo_opportunityid = 34508'
# oppo_vendorequipment = db.execute(sql).fetchone()

# jsn = json.loads(oppo_vendorequipment[0])



# print(df['desc'][25] == '1616-2846\nAvenge Pro CS04GL \nDo everything stain remover\n')

# df['matched_oids'] = []
#df.assign(matched_oids = lambda x: [])

matched_oids_list = []
for x in range(775):
    matched_oids_list += [[]]
#

df['matched_oids'] = matched_oids_list

# Objective outputs: 
#  - A list of input "Schedule A. Type/Sub-type Rules" along with the matched Opportunity IDs
#  - A list of "Scheudle A. SN + Desc's" (with Opportunity IDs) that don't appear on the input "Rules"
#  - A list of recommended "Scheudle A. JSON" changes based on input "Rules"
#  - [An option to apply recommended "Scheudle A. JSON" changes]


# Create a dictionary that map "Rules" as SN + Desc -> Type + SubType
equip_rule_lookup = {}
equip_rule_lookup2d = {}
# equip_rule_lookup[(<SN>, <Desc>)] = ("Type, "Sub-Type", )

for ea_index in df.index:
    equip_rule_lookup[(
        df['sn'][ea_index]
        , df['desc'][ea_index]
    )] = (
         df['type'][ea_index]
        , df['subtype'][ea_index]
        , []
    )
    if not df['sn'][ea_index] in equip_rule_lookup2d:
        equip_rule_lookup2d[df['sn'][ea_index]] = {}
    equip_rule_lookup2d[
        df['sn'][ea_index]
    ][
        df['desc'][ea_index]
    ] =     [(
        df['sn'][ea_index]
        , df['desc'][ea_index]
    )]
#



equip_list = get_equip()
# print(equip_list)

# ['sn','desc','type','subtype']

list_of_unruled_equipment = []

list_of_unsafe_updates = []

list_of_recommended_updates = []
list_of_verified_equipment = []


# Iterate through each Equip. JSON + Opportunity ID pair
for (ea_json_dict, ea_oid) in equip_list:
    # Track if there are any updated needed on the current OID
    ea_oppo_needs_update = False
    # Iterate through each seller equipment list
    for ea_seller_eq_list in ea_json_dict:
        # remove any empty equipment items
        if {} in ea_seller_eq_list:
            ea_seller_eq_list.remove({})
            # ea_oppo_needs_update = True
        #
        # Iterate through each equipment asset
        for ea_asset in ea_seller_eq_list:
            # Skip anything missing a Description or SN
            if not 'desc' in ea_asset:
                continue
            if not 'sn' in ea_asset:
                continue
            # Lookup the desc+SN in the rule set
            ea_key = (ea_asset['sn'], ea_asset['desc'])
            # If it's not found
            if not ea_key in equip_rule_lookup:
                # Add it to the 'unruled' list and continue
                list_of_unruled_equipment += [{
                    'ea_asset': ea_asset
                    , "ea_oid": ea_oid
                }]
                continue
            # If it is found..
            matched_type = equip_rule_lookup[ea_key][0]
            matched_subtype = equip_rule_lookup[ea_key][1]
            matched_oid_list = equip_rule_lookup[ea_key][2]
            # Record it as a matched opportunity ID on this Rule
            if not ea_oid in matched_oid_list:
                matched_oid_list += [ea_oid]
            #
            # Check if the rule conflicts with this equipment
            conflict_description = None
            current_type = None
            current_subtype = None

            if not 'type' in ea_asset:
                conflict_description = "Missing 'type' on CRM Equipment"
            else:
                current_type = ea_asset['type']
                if not current_type == matched_type:
                    conflict_description = "Missmatching 'type' on CRM Equipment"
            if not 'subtype' in ea_asset:
                conflict_description = "Missing 'subtype' on CRM Equipment"
            else:
                current_subtype = ea_asset['subtype']
                if not current_subtype == matched_subtype:
                    conflict_description = "Missmatching 'subtype' on CRM Equipment"
                #
            #
            if conflict_description:
                is_safe_to_update = (
                    current_type in [None, 'Other', '']
                ) and (
                    current_subtype in [None, 'Other', '']
                )
                if is_safe_to_update:
                    list_of_recommended_updates += [{
                        'ea_asset': ea_asset
                        , "ea_oid": ea_oid
                        , "current_type": current_type
                        , "current_subtype": current_subtype
                        , "matched_type": matched_type
                        , "matched_subtype": matched_subtype
                        , "conflict_description": conflict_description
                    }]
                    ea_oppo_needs_update = True
                    ea_asset['type'] = matched_type
                    ea_asset['subtype'] = matched_subtype
                else:
                    list_of_unsafe_updates += [{
                        'ea_asset': ea_asset
                        , "ea_oid": ea_oid
                        , "current_type": current_type
                        , "current_subtype": current_subtype
                        , "matched_type": matched_type
                        , "matched_subtype": matched_subtype
                        , "conflict_description": conflict_description
                    }]
                # - if is_safe_to_update:
            else:
                # If there is no conflict.. 
                list_of_verified_equipment += [{
                    'ea_asset': ea_asset
                    , "ea_oid": ea_oid
                    , "current_type": current_type
                    , "current_subtype": current_subtype
                    , "matched_type": matched_type
                    , "matched_subtype": matched_subtype
                }]
            # - if conflict_description:
        # - for ea_asset in ea_seller_eq_list:
    # - for ea_seller_eq_list in ea_json_dict:
    if ea_oppo_needs_update:
        # Stringify the json dictionary and run an SQL update
        update_oppo_vend_equipment(json.dumps(ea_json_dict), ea_oid)
        pass
    #
# - for (ea_json_dict, ea_oid) in equip_list:


# Load the Opporunity IDs back from the Dictionary onto the DataFrames
for ea_index in df.index:
    ea_ruledata = equip_rule_lookup[(
        df['sn'][ea_index]
        , df['desc'][ea_index]
    )][2]
    df['matched_oids'][ea_index] = ea_ruledata
#



BATCH_ID = os.urandom(8).hex()

# Output the dataframes for the rules
writer = pd.ExcelWriter('output - rule stats - '+BATCH_ID+'.xlsx')
df.to_excel(writer, 'sheet')
writer.save()


# Output the artifact lists..



with open('output - unruled equip list - '+BATCH_ID+'.json.txt', 'a') as fh:
    for ea_line in list_of_unruled_equipment:
        fh.write(json.dumps(ea_line)+"\n")

with open('output - rec update equip list - '+BATCH_ID+'.json.txt', 'a') as fh:
    for ea_line in list_of_recommended_updates:
        fh.write(json.dumps(ea_line)+"\n")

with open('output - unsafe update equip list - '+BATCH_ID+'.json.txt', 'a') as fh:
    for ea_line in list_of_unsafe_updates:
        fh.write(json.dumps(ea_line)+"\n")

with open('output - verified equip list - '+BATCH_ID+'.json.txt', 'a') as fh:
    for ea_line in list_of_verified_equipment:
        fh.write(json.dumps(ea_line)+"\n")


exit()




# test = [[[(sub_sub_element['sn'], sub_sub_element['desc']) for sub_sub_element in sub_element if 'sn' in sub_sub_element and 'desc' in sub_sub_element] for sub_element in element] for element in equip_list[0]]

# print(equip_list)

# append_to_any_txt(test)

# def f(row):
    # if row['sn'] in 





# print(test)
#df['uppercase_name'] = df['name'].apply(lambda x: x.upper())


# writer = pd.ExcelWriter('output.xlsx')
# df.to_excel(writer, 'sheet')
# writer.save()


# for ind in df.index:

'''
def database_update(row):
    global db, dbc

    dbc = odbc_connect_CRM()
    db = dbc.cursor()
    
    if row['sn'] and row['desc']:
        sql = 'select oppo_vendorequipment, oppo_opportunityid from opportunity where oppo_vendorequipment like ? or oppo_vendorequipment like ?'
        param = (f'%{row["sn"]}%', f'%{row["desc"]}%')
    elif row['sn'] and not row['desc']:
        sql = 'select oppo_vendorequipment, oppo_opportunityid from opportunity where oppo_vendorequipment like ?'
        param = f'%{row["sn"]}%'
    elif not row['sn'] and row['desc']:
        sql = 'select oppo_vendorequipment, oppo_opportunityid from opportunity where oppo_vendorequipment like ?'
        param = f'%{row["desc"]}%'
    else:
        print('neither is true')
        return

    json_str = db.execute(sql, param).fetchone()
    if json_str:
        json_ld = json.loads(json_str[0])
        oppo_id = json_str[1]
        # print(oppo_id)
        append_1(json_ld, row['sn'], row['desc'], row['type'], row['subtype'], oppo_id)
    else:
        append_to_not_found_list(row)
'''
# excel_file = pd.read_excel('C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/AT191/report.xlsx', sheet_name='Schedule A (Final)')

# df = pd.DataFrame(excel_file, columns = ['comp_companyid','comp_name','oppo_status','oppo_approval_lender',
# 'oppo_approval_txntype','oppo_fund_expDate','oppo_vendorlist','qty','sn','desc','cost','cond','year',
# 'make','model','miles','titled','type','subtype','estdevdate','confdevdate'])

# excel_file = pd.read_excel('C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/AT191/report.xlsx', sheet_name='Sheet2')

# df = pd.DataFrame(excel_file, columns = ['sn','desc','type','subtype'])

# df = df.fillna('')

# equips = get_equip()
# print(equips)

# for ind in df.index:
#     if(df['type'][ind] and df['subtype'][ind]):
#         if(df['sn'][ind] or df['desc'][ind]):
#             database_update(df.iloc[ind])
#         else:
#             append_to_not_found_list(df.iloc[ind])
#     elif(not df['type'][ind] and not df['subtype'][ind]):
#         append_to_both_missing_list(df.iloc[ind])
#     else:
#         append_to_one_missing_list(df.iloc[ind])
