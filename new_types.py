import pandas as pd
import json
from datetime import datetime

excel_file = pd.read_excel('C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/AT347/Equipment List as of June 13.xlsx', sheet_name='Sheet1')

df = pd.DataFrame(excel_file, columns = ['Spokes','Sub groups','Equipment Type'])

df = df.fillna('')

asset_type_subtype_list = {}

for ea_ind in df.index:
    asset_type_subtype_list[df['Sub groups'][ea_ind]] = df['Equipment Type'][ea_ind]

dict_subtypes = df.groupby('Sub groups')['Equipment Type'].apply(set).apply(list).to_dict()

dict_spokes = df.groupby('Sub groups')['Spokes'].apply(set).apply(list).to_dict()

filename = "ASSET_TYPE_SUBTYPE_LIST__" + datetime.now().strftime("%Y") + "_" + datetime.now().strftime("%m") + ".json"

with open(filename, 'w', encoding='utf-8') as f:
    json.dump(dict_subtypes, f, ensure_ascii=False, indent=4)
