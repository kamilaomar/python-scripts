import os
import shutil

def list_of_files_from_logs():
     directory = 'C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/Logs/python-scripts'

     lines = []

     list_of_filenames = [x for x in os.listdir(directory) if '.log' in x]

     for filename in list_of_filenames:
          with open(filename, 'r+', errors = 'ignore') as f:
               log_file = f.read()
          lines += log_file.split('\n')

     lines = [line for line in lines if '/' in line]
     lines = [line.split('/', 1)[1] for line in lines]
     lines = [line.split(' ', 1)[0] for line in lines]

     lines = list(set(lines))

     return lines

dir_of_walk = '//crmsandbox/e$/Program Files/Sage/CRM/CRM/WWWRoot/CustomPages'
# dir_of_walk = 'C:/Users/KamilaOmar/OneDrive - Taycor Financial/Desktop/templates/window'

def list_of_included_files():

     lines = []

     for root, dirs, files in os.walk(dir_of_walk):
          for filename in files:
               if '.asp' in filename:
                    with open(os.path.join(root, filename), errors = 'ignore') as f:
                         log_file = f.read()
                    lines += log_file.split('\n')

     includes = [line for line in lines if 'include' in line]
               
     return includes

def move_files(src, dst):
     try:
          shutil.move(src, dst)
     except FileNotFoundError:
          directory = dst.rsplit('/', 1)[0]
          os.makedirs(directory)
          shutil.move(src, dst)


needed_files_arr = list_of_included_files() + list_of_files_from_logs()
f = open("files_array.txt", "a")
for x in needed_files_arr:
     f.writelines(x + '\n')
f.close()

def print_to_csv(file_name, list_of_elem):
    from csv import writer

    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

BATCH_ID = os.urandom(8).hex()

for root, dirs, files in os.walk(dir_of_walk):
     for filename in files:
          if any([filename in x for x in needed_files_arr]):
               pass
          else:
               src_path = os.path.join(root, filename)
               dst_path = src_path.split('\\', 1)[0] + "/TEMP_REMOVED/" + src_path.split('\\', 1)[1]
               src_path = src_path.replace('\\', '/')
               dst_path = dst_path.replace('\\', '/')

               if(('doc' not in src_path) and ('call-lists' not in src_path) and ('TEMP_REMOVED' not in src_path)):

                    print_to_csv(f'files_to_move_{BATCH_ID}.csv', [src_path, dst_path])
                    # move_files(src_path, dst_path)
