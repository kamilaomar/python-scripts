import csv
import shutil
import os

def move_files(src, dst):
     try:
          shutil.move(src, dst)
     except FileNotFoundError:
          directory = dst.rsplit('/', 1)[0]
          os.makedirs(directory)
          shutil.move(src, dst)


filename = "files1.csv"

rows = []

with open(filename, 'r') as csvfile:
    csvreader = csv.reader(csvfile)

    for row in csvreader:
        rows.append(row)


for row in rows:
    move_files(row[0], row[1])